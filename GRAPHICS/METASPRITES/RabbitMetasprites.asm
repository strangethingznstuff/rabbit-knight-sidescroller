rabbit_0_data:

	.byte   0,  0,$00,0
	.byte   8,  0,$01,0
	.byte  16,  0,$02,0
	.byte   0,  8,$03,0
	.byte   8,  8,$04,0
	.byte  16,  8,$05,0
	.byte   8, 16,$06,0
	.byte  16, 16,$07,0
	.byte   0, 24,$08,0
	.byte   8, 24,$09,0
	.byte  16, 24,$0a,0
	.byte  24, 24,$0b,0
	.byte   0, 32,$0c,0
	.byte   8, 32,$0d,0
	.byte  16, 32,$0e,0
	.byte   8, 40,$0f,0
	.byte  16, 40,$10,0
	.byte 128

rabbit_1_data:

	.byte   8,  0,$11,0
	.byte  16,  0,$12,0
	.byte   8,  8,$13,0
	.byte  16,  8,$14,0
	.byte   8, 16,$15,0
	.byte  16, 16,$16,0
	.byte   0, 24,$08,0
	.byte   8, 24,$17,0
	.byte  16, 24,$18,0
	.byte  24, 24,$0b,0
	.byte   0, 32,$0c,0
	.byte   8, 32,$0d,0
	.byte  16, 32,$0e,0
	.byte   8, 40,$19,0
	.byte  16, 40,$1a,0
	.byte 128

rabbit_2_data:

	.byte   8,  0,$11,0
	.byte  16,  0,$1b,0
	.byte  24,  0,$1c,0
	.byte   8,  8,$13,0
	.byte  16,  8,$1d,0
	.byte  24,  8,$1e,0
	.byte   8, 16,$1f,0
	.byte  16, 16,$20,0
	.byte  24, 16,$1e,0
	.byte   0, 24,$21,0
	.byte   8, 24,$22,0
	.byte  16, 24,$23,0
	.byte  24, 24,$24,0
	.byte   0, 32,$25,0
	.byte   8, 32,$26,0
	.byte  16, 32,$27,0
	.byte  24, 32,$28,0
	.byte   8, 40,$29,0
	.byte  16, 40,$2a,0
	.byte  24, 40,$2b,0
	.byte 128

rabbit_3_data:

	.byte   8,  0,$11,0
	.byte  16,  0,$12,0
	.byte   8,  8,$13,0
	.byte  16,  8,$14,0
	.byte   8, 16,$1f,0
	.byte  16, 16,$2c,0
	.byte   0, 24,$08,0
	.byte   8, 24,$17,0
	.byte  16, 24,$2d,0
	.byte  24, 24,$0b,0
	.byte   0, 32,$0c,0
	.byte   8, 32,$0d,0
	.byte  16, 32,$0e,0
	.byte   8, 40,$0f,0
	.byte  16, 40,$10,0
	.byte 128

rabbit_4_data:

	.byte   8,  1,$11,0
	.byte  16,  1,$2e,0
	.byte   8,  9,$13,0
	.byte  16,  9,$2f,0
	.byte   0, 17,$30,0
	.byte   8, 17,$1f,0
	.byte  16, 17,$31,0
	.byte   0, 25,$32,0
	.byte   8, 25,$33,0
	.byte  16, 25,$34,0
	.byte   0, 33,$35,0
	.byte   8, 33,$36,0
	.byte  16, 33,$37,0
	.byte   8, 41,$38,0
	.byte  16, 41,$39,0
	.byte 128

rabbit_5_data:

	.byte   8,  1,$11,0
	.byte  16,  1,$2e,0
	.byte   8,  9,$13,0
	.byte  16,  9,$3a,0
	.byte  24,  9,$2b,0|OAM_FLIP_V
	.byte   8, 17,$1f,0
	.byte  16, 17,$3b,0
	.byte  24, 17,$3c,0
	.byte   0, 25,$3d,0
	.byte   8, 25,$3e,0
	.byte  16, 25,$3f,0
	.byte  24, 25,$40,0
	.byte   0, 33,$41,0
	.byte   8, 33,$42,0
	.byte  16, 33,$43,0
	.byte  24, 33,$44,0
	.byte   8, 41,$45,0
	.byte  16, 41,$46,0
	.byte  24, 41,$00,0|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

rabbit_6_data:

	.byte   8,  0,$47,0
	.byte  16,  0,$35,0|OAM_FLIP_H|OAM_FLIP_V
	.byte   0,  8,$48,0
	.byte   8,  8,$49,0
	.byte  16,  8,$4a,0
	.byte  24,  8,$4b,0
	.byte   8, 16,$4c,0
	.byte  16, 16,$4d,0
	.byte  24, 16,$3c,0
	.byte   0, 24,$4e,0
	.byte   8, 24,$4f,0
	.byte  16, 24,$50,0
	.byte  24, 24,$40,0
	.byte   0, 32,$51,0
	.byte   8, 32,$52,0
	.byte  16, 32,$53,0
	.byte  24, 32,$44,0
	.byte   0, 40,$54,0
	.byte   8, 40,$55,0
	.byte  16, 40,$56,0
	.byte  24, 40,$00,0|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

rabbit_7_data:

	.byte   8,  0,$57,0
	.byte   0,  8,$48,0
	.byte   8,  8,$58,0
	.byte  16,  8,$59,0
	.byte  24,  8,$5a,0
	.byte   8, 16,$5b,0
	.byte  16, 16,$5c,0
	.byte  24, 16,$3c,0
	.byte   0, 24,$5d,0
	.byte   8, 24,$5e,0
	.byte  16, 24,$50,0
	.byte  24, 24,$40,0
	.byte   0, 32,$5f,0
	.byte   8, 32,$60,0
	.byte  16, 32,$61,0
	.byte  24, 32,$44,0
	.byte   0, 40,$62,0
	.byte   8, 40,$63,0
	.byte  16, 40,$64,0
	.byte  24, 40,$00,0|OAM_FLIP_H|OAM_FLIP_V
	.byte 128

rabbit_8_data:

	.byte  24,  0,$00,0|OAM_FLIP_H
	.byte  16,  0,$01,0|OAM_FLIP_H
	.byte   8,  0,$02,0|OAM_FLIP_H
	.byte  24,  8,$03,0|OAM_FLIP_H
	.byte  16,  8,$04,0|OAM_FLIP_H
	.byte   8,  8,$05,0|OAM_FLIP_H
	.byte  16, 16,$06,0|OAM_FLIP_H
	.byte   8, 16,$07,0|OAM_FLIP_H
	.byte  24, 24,$08,0|OAM_FLIP_H
	.byte  16, 24,$09,0|OAM_FLIP_H
	.byte   8, 24,$0a,0|OAM_FLIP_H
	.byte   0, 24,$0b,0|OAM_FLIP_H
	.byte  24, 32,$0c,0|OAM_FLIP_H
	.byte  16, 32,$0d,0|OAM_FLIP_H
	.byte   8, 32,$0e,0|OAM_FLIP_H
	.byte  16, 40,$0f,0|OAM_FLIP_H
	.byte   8, 40,$10,0|OAM_FLIP_H
	.byte 128

rabbit_9_data:

	.byte  16,  0,$11,0|OAM_FLIP_H
	.byte   8,  0,$12,0|OAM_FLIP_H
	.byte  16,  8,$13,0|OAM_FLIP_H
	.byte   8,  8,$14,0|OAM_FLIP_H
	.byte  16, 16,$15,0|OAM_FLIP_H
	.byte   8, 16,$16,0|OAM_FLIP_H
	.byte  24, 24,$08,0|OAM_FLIP_H
	.byte  16, 24,$17,0|OAM_FLIP_H
	.byte   8, 24,$18,0|OAM_FLIP_H
	.byte   0, 24,$0b,0|OAM_FLIP_H
	.byte  24, 32,$0c,0|OAM_FLIP_H
	.byte  16, 32,$0d,0|OAM_FLIP_H
	.byte   8, 32,$0e,0|OAM_FLIP_H
	.byte  16, 40,$19,0|OAM_FLIP_H
	.byte   8, 40,$1a,0|OAM_FLIP_H
	.byte 128

rabbit_10_data:

	.byte  16,  0,$11,0|OAM_FLIP_H
	.byte   8,  0,$1b,0|OAM_FLIP_H
	.byte   0,  0,$1c,0|OAM_FLIP_H
	.byte  16,  8,$13,0|OAM_FLIP_H
	.byte   8,  8,$1d,0|OAM_FLIP_H
	.byte   0,  8,$1e,0|OAM_FLIP_H
	.byte  16, 16,$1f,0|OAM_FLIP_H
	.byte   8, 16,$20,0|OAM_FLIP_H
	.byte   0, 16,$1e,0|OAM_FLIP_H
	.byte  24, 24,$21,0|OAM_FLIP_H
	.byte  16, 24,$22,0|OAM_FLIP_H
	.byte   8, 24,$23,0|OAM_FLIP_H
	.byte   0, 24,$24,0|OAM_FLIP_H
	.byte  24, 32,$25,0|OAM_FLIP_H
	.byte  16, 32,$26,0|OAM_FLIP_H
	.byte   8, 32,$27,0|OAM_FLIP_H
	.byte   0, 32,$28,0|OAM_FLIP_H
	.byte  16, 40,$29,0|OAM_FLIP_H
	.byte   8, 40,$2a,0|OAM_FLIP_H
	.byte   0, 40,$2b,0|OAM_FLIP_H
	.byte 128

rabbit_11_data:

	.byte  16,  0,$11,0|OAM_FLIP_H
	.byte   8,  0,$12,0|OAM_FLIP_H
	.byte  16,  8,$13,0|OAM_FLIP_H
	.byte   8,  8,$14,0|OAM_FLIP_H
	.byte  16, 16,$1f,0|OAM_FLIP_H
	.byte   8, 16,$2c,0|OAM_FLIP_H
	.byte  24, 24,$08,0|OAM_FLIP_H
	.byte  16, 24,$17,0|OAM_FLIP_H
	.byte   8, 24,$2d,0|OAM_FLIP_H
	.byte   0, 24,$0b,0|OAM_FLIP_H
	.byte  24, 32,$0c,0|OAM_FLIP_H
	.byte  16, 32,$0d,0|OAM_FLIP_H
	.byte   8, 32,$0e,0|OAM_FLIP_H
	.byte  16, 40,$0f,0|OAM_FLIP_H
	.byte   8, 40,$10,0|OAM_FLIP_H
	.byte 128

rabbit_12_data:

	.byte  16,  1,$11,0|OAM_FLIP_H
	.byte   8,  1,$2e,0|OAM_FLIP_H
	.byte  16,  9,$13,0|OAM_FLIP_H
	.byte   8,  9,$2f,0|OAM_FLIP_H
	.byte  24, 17,$30,0|OAM_FLIP_H
	.byte  16, 17,$1f,0|OAM_FLIP_H
	.byte   8, 17,$31,0|OAM_FLIP_H
	.byte  24, 25,$32,0|OAM_FLIP_H
	.byte  16, 25,$33,0|OAM_FLIP_H
	.byte   8, 25,$34,0|OAM_FLIP_H
	.byte  24, 33,$35,0|OAM_FLIP_H
	.byte  16, 33,$36,0|OAM_FLIP_H
	.byte   8, 33,$37,0|OAM_FLIP_H
	.byte  16, 41,$38,0|OAM_FLIP_H
	.byte   8, 41,$39,0|OAM_FLIP_H
	.byte 128

rabbit_13_data:

	.byte  16,  1,$11,0|OAM_FLIP_H
	.byte   8,  1,$2e,0|OAM_FLIP_H
	.byte  16,  9,$13,0|OAM_FLIP_H
	.byte   8,  9,$3a,0|OAM_FLIP_H
	.byte   0,  9,$2b,0|OAM_FLIP_H|OAM_FLIP_V
	.byte  16, 17,$1f,0|OAM_FLIP_H
	.byte   8, 17,$3b,0|OAM_FLIP_H
	.byte   0, 17,$3c,0|OAM_FLIP_H
	.byte  24, 25,$3d,0|OAM_FLIP_H
	.byte  16, 25,$3e,0|OAM_FLIP_H
	.byte   8, 25,$3f,0|OAM_FLIP_H
	.byte   0, 25,$40,0|OAM_FLIP_H
	.byte  24, 33,$41,0|OAM_FLIP_H
	.byte  16, 33,$42,0|OAM_FLIP_H
	.byte   8, 33,$43,0|OAM_FLIP_H
	.byte   0, 33,$44,0|OAM_FLIP_H
	.byte  16, 41,$45,0|OAM_FLIP_H
	.byte   8, 41,$46,0|OAM_FLIP_H
	.byte   0, 41,$00,0|OAM_FLIP_V
	.byte 128

rabbit_14_data:

	.byte  16,  0,$47,0|OAM_FLIP_H
	.byte   8,  0,$35,0|OAM_FLIP_V
	.byte  24,  8,$48,0|OAM_FLIP_H
	.byte  16,  8,$49,0|OAM_FLIP_H
	.byte   8,  8,$4a,0|OAM_FLIP_H
	.byte   0,  8,$4b,0|OAM_FLIP_H
	.byte  16, 16,$4c,0|OAM_FLIP_H
	.byte   8, 16,$4d,0|OAM_FLIP_H
	.byte   0, 16,$3c,0|OAM_FLIP_H
	.byte  24, 24,$4e,0|OAM_FLIP_H
	.byte  16, 24,$4f,0|OAM_FLIP_H
	.byte   8, 24,$50,0|OAM_FLIP_H
	.byte   0, 24,$40,0|OAM_FLIP_H
	.byte  24, 32,$51,0|OAM_FLIP_H
	.byte  16, 32,$52,0|OAM_FLIP_H
	.byte   8, 32,$53,0|OAM_FLIP_H
	.byte   0, 32,$44,0|OAM_FLIP_H
	.byte  24, 40,$54,0|OAM_FLIP_H
	.byte  16, 40,$55,0|OAM_FLIP_H
	.byte   8, 40,$56,0|OAM_FLIP_H
	.byte   0, 40,$00,0|OAM_FLIP_V
	.byte 128

rabbit_15_data:

	.byte  16,  0,$57,0|OAM_FLIP_H
	.byte  24,  8,$48,0|OAM_FLIP_H
	.byte  16,  8,$58,0|OAM_FLIP_H
	.byte   8,  8,$59,0|OAM_FLIP_H
	.byte   0,  8,$5a,0|OAM_FLIP_H
	.byte  16, 16,$5b,0|OAM_FLIP_H
	.byte   8, 16,$5c,0|OAM_FLIP_H
	.byte   0, 16,$3c,0|OAM_FLIP_H
	.byte  24, 24,$5d,0|OAM_FLIP_H
	.byte  16, 24,$5e,0|OAM_FLIP_H
	.byte   8, 24,$50,0|OAM_FLIP_H
	.byte   0, 24,$40,0|OAM_FLIP_H
	.byte  24, 32,$5f,0|OAM_FLIP_H
	.byte  16, 32,$60,0|OAM_FLIP_H
	.byte   8, 32,$61,0|OAM_FLIP_H
	.byte   0, 32,$44,0|OAM_FLIP_H
	.byte  24, 40,$62,0|OAM_FLIP_H
	.byte  16, 40,$63,0|OAM_FLIP_H
	.byte   8, 40,$64,0|OAM_FLIP_H
	.byte   0, 40,$00,0|OAM_FLIP_V
	.byte 128

rabbit_pointers:

	.word rabbit_0_data
	.word rabbit_1_data
	.word rabbit_2_data
	.word rabbit_3_data
	.word rabbit_4_data
	.word rabbit_5_data
	.word rabbit_6_data
	.word rabbit_7_data
	.word rabbit_8_data
	.word rabbit_9_data
	.word rabbit_10_data
	.word rabbit_11_data
	.word rabbit_12_data
	.word rabbit_13_data
	.word rabbit_14_data
	.word rabbit_15_data

