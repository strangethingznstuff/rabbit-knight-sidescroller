# Rabbit Knight Sidescroller (NES)

Written by Nasario Cruz (Soulite)

## Description

This is a prototype for a sidescroller for the Nintendo Entertainment System. Written fully in 6502 assembly language, for compilation with the cc65 suite.

The demo showcases dynamic loading of map data interprited as PPU tile and attribute data, while allowing for seemless transitions between map portions (loading columns as they are needed).

There is also tile collision for the player character, with some tiles being solid, and others being passable.

Besides automatically generated data in assembly format, such as for the metasprites (collections of tiles and their relative positions used for the characters). All assembly code is completely custom and original. All graphics are original as well, besides a color palette partially derived from the original Super Mario Brothers.

## Building and Running

To build the game on windows (with cc65 installed and in PATH), simply run "make.bat". This will generate an NES ROM file called "game.nes".

To run the game, load the file into your NES emulator of choice, such as Mesen.

## Software Used

- cc65 suite
- NES Screen Tool
- Tilificator
- yychr
