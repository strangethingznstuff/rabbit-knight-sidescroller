;;SPRITE OAM
  LDA #$02 ; Copy sprite data from page $0200 into PPU
  STA OAMDMA







;;Load nametable column
  LDA mapLoadTemp
  CMP #$FF
  BNE :+
  JSR LoadMetatileColumnNMI
:








;;Test load tile
  ;Set up Increment mode to +32
  LDA ppuCtrlSet
  AND #%11111011
  CLC
  ADC #%00000100
  STA PPUCTRL
  STA ppuCtrlSet






;LOAD TILES FROM STACK

;IF TileStack is empty don't render
  LDA tileStackPointer
  CMP #$FF
  BNE :+
  JMP EndLoadBigTilePPULoop
:

;Loop Condition
  LDX #$00
LoadBigTilePPULoop:  
;Loop Body
  BIT PPUSTATUS ;Reset High/Low Byte
  ;Load first tile position in BigTile into PPUADDR
  LDA TileStack+BigTile::addressHigh, X
  STA PPUADDR
  LDA TileStack+BigTile::addressLow, X
  STA PPUADDR
  
  LDA TileStack+BigTile::tile0, X
  STA PPUDATA
  LDA TileStack+BigTile::tile1, X
  STA PPUDATA
  LDA tileStackPointer
  SEC
  SBC #$04

  BCS :+
  LDA #$FF
  STA tileStackPointer
  JMP EndLoadBigTilePPULoop
:
  STA tileStackPointer
  INX
  INX
  INX
  INX
  JMP LoadBigTilePPULoop

EndLoadBigTilePPULoop:  





;LOAD COLORS FROM STACK
;IF ColorStack is empty don't render
  LDA colorStackPointer
  CMP #$FF
  BNE :+
  JMP EndLoadTileColorPPULoop
:


;Loop Condition
  LDX #$00
LoadTileColorPPULoop:
;Loop Body
  BIT PPUSTATUS ;Reset High/Low Byte
  ;Load first color position in TileColor into PPUADDR
  LDA ColorStack+NametableColor::addressHigh, X
  STA PPUADDR
  LDA ColorStack+NametableColor::addressLow, X
  STA PPUADDR
  
  LDA ColorStack+NametableColor::color, X
  STA PPUDATA
  
  LDA colorStackPointer
  SEC
  SBC #$04

  BCS :+
  LDA #$FF
  STA colorStackPointer
  JMP EndLoadTileColorPPULoop
:
  STA colorStackPointer
  INX
  INX
  INX
  INX
  JMP LoadTileColorPPULoop

EndLoadTileColorPPULoop:  










;;SET BIG SCROLL OFFSET
;Set up scroll X in PPUCTRL
LDA cameraX+1
AND #%00000001
BNE :+

;;Set to Nametable 0
LDA ppuCtrlSet
AND #%11111100
STA PPUCTRL
JMP :++
:
;;Set to Nametable 1
LDA ppuCtrlSet
AND #%11111100
CLC
ADC #%00000001
STA PPUCTRL
:








;;SET FINE SCROLL OFFSET
;Set fine scroll Pos
BIT PPUSTATUS ;Reset X/Y order
LDA cameraX ;Fine scroll is same as camera position
STA PPUSCROLL
LDA #$00 ;(Y offset is zero, no vertical scrolling)
STA PPUSCROLL