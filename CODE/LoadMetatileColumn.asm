
;Load World
  ;Store World Data pointer to "world"

  LDA mapLoadX
  CLC
  ROL
  CLC
  ADC #$00
  TAX
  LDA Level1MapPointers, X
  STA world
  LDA Level1MapPointers+1, X
  STA world+1






  LDY mapLoadColumn


  STY mapLoadTemp ;Back up column Tile (8x8) position
  TYA
  CLC
  ROR ;Get Metatile column position
  TAY


;;Load in column of map data
  LDX #$00
  LDA #%00000001
  BIT mapLoadX
  BNE :+
  JMP MapCoulumn1CopyLoop
:



  ;Copy map data to MapData2
MapCoulumn2CopyLoop:

  
  ;If rightmost column of 4, load previous map column for ATTR loading
  LDA mapLoadColumn
  AND #%00000011
  CMP #$03
  BNE SkipLoadLeftColumn2SC

  ;LOAD LEFT MAP COLUMN -1
  DEY
  CPY #$FF
  BNE :+
  LDY #$0F
:

:
  LDA (world), Y
  STA MapData2, Y
  TYA
  CLC
  ADC #$10
  TAY
  INX
  CPX #$20
  BNE :-
  INY
  JMP LoadMiddleColumn2SC
SkipLoadLeftColumn2SC:

  ;If leftmost column of 4, load next map column for ATTR loading
  LDA mapLoadColumn
  AND #%00000011
  CMP #$00
  BNE SkipLoadRightColumn2SC

  ;LOAD RIGHT MAP COLUMN +1
  INY
:
  LDA (world), Y
  STA MapData2, Y
  TYA
  CLC
  ADC #$10
  TAY
  INX
  CPX #$20
  BNE :-
  DEY
SkipLoadRightColumn2SC:



LoadMiddleColumn2SC:
  LDX #$00
  ;LOAD MIDDLE (CHOSEN) MAP COLUMN +0
:
  LDA (world), Y
  STA MapData2, Y
  TYA
  CLC
  ADC #$10
  TAY
  INX
  CPX #$20
  BNE :-


























  ;Set world High byte to MapData2
  LDA #$05
  STA world+1

  LDA #$00
  STA world

  JMP ColumnLoadChecker
  ;Copy map data to MapData1
MapCoulumn1CopyLoop:

  
  ;If rightmost column of 4, load previous map column for ATTR loading
  LDA mapLoadColumn
  AND #%00000011
  CMP #$03
  BNE SkipLoadLeftColumn1SC

  ;LOAD LEFT MAP COLUMN -1
  DEY
  CPY #$FF
  BNE :+
  LDY #$0F
:

:
  LDA (world), Y
  STA MapData1, Y
  TYA
  CLC
  ADC #$10
  TAY
  INX
  CPX #$20
  BNE :-
  INY
  JMP LoadMiddleColumn1SC
SkipLoadLeftColumn1SC:

  ;If leftmost column of 4, load next map column for ATTR loading
  LDA mapLoadColumn
  AND #%00000011
  CMP #$00
  BNE SkipLoadRightColumn1SC

  ;LOAD RIGHT MAP COLUMN +1
  INY
:
  LDA (world), Y
  STA MapData1, Y
  TYA
  CLC
  ADC #$10
  TAY
  INX
  CPX #$20
  BNE :-
  DEY
SkipLoadRightColumn1SC:



LoadMiddleColumn1SC:
  LDX #$00
  ;LOAD MIDDLE (CHOSEN) MAP COLUMN +0
:
  LDA (world), Y
  STA MapData1, Y
  TYA
  CLC
  ADC #$10
  TAY
  INX
  CPX #$20
  BNE :-






















  ;Set world High byte to MapData1
  LDA #$04
  STA world+1

  LDA #$00
  STA world











ColumnLoadChecker:

;;Check which column to load
  LDA #%00000001
  BIT mapLoadTemp
  BNE :+
  ;If Left Column
  LDX #$00
  LDA mapLoadTemp
  CLC
  ROR ;
  TAY
  JMP LoadLeftColumnMT
:
  ;If Left Column
  LDX #$00
  LDA mapLoadTemp
  CLC
  ROR ;
  TAY
  JMP LoadRightColumnMT
  

LoadLeftColumnMT:;Load metatiles into MapColumnTiles
  
  LDA (world), Y
  STA MapColumnTiles, X
  STA MapColumnTiles+1, X
  TYA
  CLC
  ADC #$10
  TAY
  INX
  INX
  CPX #$20
  BNE LoadLeftColumnMT


  LDY #$00
LoadLeftColumnTilesMT:     ;Y REG IS Y POSITION
  LDA #$00 ;Reset mapLoadTemp
  STA mapLoadTemp
  
  LDX #$00 ;TOP LEFT TILE
  LDA MapColumnTiles, Y

  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
  CLC
  ;ADC #$01 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:
  STA MapColumnTiles, Y
  INY




  LDA MapColumnTiles, Y

  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
  CLC
  ADC #$01 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:
  STA MapColumnTiles, Y
  INY






  
  CPY #$20
  BEQ :+ 
  JMP LoadLeftColumnTilesMT
:
  JMP EndLoadMetatileColumn



LoadRightColumnMT:;Load metatiles into MapColumnTiles
  
  LDA (world), Y
  STA MapColumnTiles, X
  STA MapColumnTiles+1, X
  TYA
  CLC
  ADC #$10
  TAY
  INX
  INX
  CPX #$20
  BNE LoadRightColumnMT


  LDY #$00
LoadRightColumnTilesMT:     ;Y REG IS Y POSITION
  LDA #$00 ;Reset mapLoadTemp
  STA mapLoadTemp
  
  LDX #$00 ;TOP LEFT TILE
  LDA MapColumnTiles, Y

  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
  CLC
  ADC #$02 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:
  STA MapColumnTiles, Y
  INY




  LDA MapColumnTiles, Y

  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
  CLC
  ADC #$03 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:
  STA MapColumnTiles, Y
  INY






  
  CPY #$20
  BEQ :+ 
  JMP LoadRightColumnTilesMT
:
EndLoadMetatileColumn:





































  LDA #$00
  STA temp


  


  LDA mapLoadColumn
  STA temp+1
  AND #%00000011
  CMP #$03
  BNE :+
  LDA mapLoadColumn
  AND #%11111100
  STA mapLoadColumn
:

  LDX #$00
LoadMetatileColumnAttributes:

  LDA #$00
  STA mapLoadTemp

  ;Store horizontal offset in temp
  LDA mapLoadColumn
  ;AND #%00000111
  CLC
  ROR ;Attr Index * 2
  ;ROR
  STA temp





  ;LOAD BOTTOM RIGHT TILE
  TXA ;Vertical position
  CLC 
  ROL
  ROL
  ROL
  ROL
  ROL ;Vertical position *32
  ADC #$11 ;Point to bottom right metatile
  CLC
  ADC temp
  TAY


  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL
  STA mapLoadTemp

  ;LOAD BOTTOM LEFT TILE
  TXA
  CLC 
  ROL
  ROL
  ROL
  ROL
  ROL
  ADC #$10 ;Point to bottom right metatile
  ADC temp
  TAY


  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL
  STA mapLoadTemp

  ;LOAD TOP RIGHT TILE
  TXA
  CLC 
  ROL
  ROL
  ROL
  ROL
  ROL
  ADC #$01 ;Point to bottom right metatile
  ADC temp
  TAY


  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL
  STA mapLoadTemp

  ;LOAD TOP LEFT TILE
  TXA
  CLC 
  ROL
  ROL
  ROL
  ROL
  ROL
  ADC #$00 ;Point to bottom right metatile
  ADC temp
  TAY


  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  STA mapLoadTemp








  STA MapColumnAttributes, X
  INX
  CPX #$07
  BEQ :+
  JMP LoadMetatileColumnAttributes
:




  LDX #$07
  LDA #$00
  STA mapLoadTemp

  ;Store horizontal offset in temp
  LDA mapLoadColumn
  ;AND #%00000111
  CLC
  ROR ;Attr Index * 2
  STA temp

  ;LOAD TOP RIGHT TILE
  TXA
  CLC 
  ROL
  ROL
  ROL
  ROL
  ROL
  ADC #$01 ;Point to bottom right metatile
  ADC temp
  TAY


  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL
  STA mapLoadTemp

  ;LOAD TOP LEFT TILE
  TXA
  CLC 
  ROL
  ROL
  ROL
  ROL
  ROL
  ADC #$00 ;Point to bottom right metatile
  ADC temp
  TAY


  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  STA mapLoadTemp

  ;LDX #$07
  STA MapColumnAttributes, X



















  LDA temp+1
  STA mapLoadColumn



  ;Set mapLoadTemp, tells it to render in NMI
  LDA #$FF
  STA mapLoadTemp

  