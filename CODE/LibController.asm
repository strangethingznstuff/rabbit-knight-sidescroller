;CONTROLLER SUBROUTINES
ReadJoy: ;READS INPUT FROM FIRST CONTROLLER
    LDA buttons1
    STA buttons1old
	LDA #$01
	STA buttons1
    ; While the strobe bit is set, buttons will be continuously reloaded.
    ; This means that reading from JOYPAD1 will only return the state of the
    ; first button: button A.
    STA $4016
	LDA #$00
	STA $4016
CtrLoop:
    LDA $4016
    LSR a	       ; bit 0 -> Carry
    ROL buttons1  ; Carry -> bit 0; bit 7 -> Carry
    BCC CtrLoop

	RTS