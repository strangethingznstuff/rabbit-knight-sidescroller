

LoadMetatileColumnNMI:
  ;Point to Nametable PPUCTRL + tile offset

;Set up Increment mode to +32
  LDA ppuCtrlSet
  AND #%11111011
  CLC
  ADC #%00000100
  STA PPUCTRL
  STA ppuCtrlSet


  LDA #%00000001
  BIT mapLoadX
  BEQ :+
  LDA #$24
  JMP :++
:
  LDA #$20 ;High byte is $20 + Nametable offset
:

  STA PPUADDR
  LDA mapLoadColumn
  STA PPUADDR


  LDX #$00
  
:
  LDA MapColumnTiles, X
  STA PPUDATA
  INX
  CPX #$1E ;30 tiles
  BNE :-








  LDA mapLoadColumn
  AND #%00000011
  CMP #$00
  BEQ :+
  CMP #$03
  BEQ :+
  JMP SkipLoadMapAttributes
:




LoadMapAttributes:
  LDX #$00

  LDA mapLoadColumn
  CLC
  ROR
  CLC
  ROR ;;Divide A by 4
  STA temp

  LDA #%00000001
  BIT mapLoadX
  BEQ LoadMapAttributesLoopLeft

LoadMapAttributesLoopRight:
  LDA #$27
  STA PPUADDR
;;LOW ADDRESSBYTE
  LDA temp
  CLC
;  ADC temp ;Add C0 offset (Attr start)
  ADC LoadMetatileColumnLowOffsetRef, X ;
  STA PPUADDR ;Store PPU Write Address low byte

;;LOAD AND STORE ATTRIBUTE DATA
  LDA MapColumnAttributes, X
  STA PPUDATA
  INX
  CPX #$08 ;for x=0, x<8, x++
  BNE LoadMapAttributesLoopRight
  JMP SkipLoadMapAttributes

LoadMapAttributesLoopLeft:
  LDA #$23
  STA PPUADDR
;;LOW ADDRESSBYTE
  LDA temp
  CLC
;  ADC temp ;Add C0 offset (Attr start)
  ADC LoadMetatileColumnLowOffsetRef, X ;
  STA PPUADDR ;Store PPU Write Address low byte

;;LOAD AND STORE ATTRIBUTE DATA
  LDA MapColumnAttributes, X
  STA PPUDATA
  INX
  CPX #$08 ;for x=0, x<8, x++
  BNE LoadMapAttributesLoopLeft
SkipLoadMapAttributes:
  RTS

LoadMetatileColumnLowOffsetRef:
  .byte $C0
  .byte $C8
  .byte $D0
  .byte $D8
  .byte $E0
  .byte $E8
  .byte $F0
  .byte $F8