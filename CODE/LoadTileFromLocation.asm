

LoadTileFromLocation: ;PARAMS:  X - X pos   Y - Y pos   Z - map 1 (0), map 2 (1)
  
  BEQ :+ ;If Zero flag set load from map 1
  JMP LoadTileFromMap2
:

LoadTileFromMap1:
  TYA
  CLC
  ROR ;ABS(Y/16) = tile Y
  CLC
  ROR
  CLC
  ROR
  CLC
  ROR
  TAY ;tile Y => Y

  TXA
  CLC
  ROR ;ABS(X/16) = tile X
  CLC
  ROR
  CLC
  ROR
  CLC
  ROR
  TAX ;tile X => X

  TYA
  CMP #$0F ;If tile Y is 15, it's not an actual tile (load 0 as replacement)
  BNE :+
  LDA #$00
  JMP EndLoadTileFromLocation
:

  TYA
  CLC
  ROL ;tile Y * 16 = tile y offset
  ROL
  ROL
  ROL
  STA temp

  TXA
  CLC
  ADC temp
  TAX
  LDA MapData1, X
  JMP EndLoadTileFromLocation

LoadTileFromMap2:
  TYA
  CLC
  ROR ;ABS(Y/16) = tile Y
  CLC
  ROR
  CLC
  ROR
  CLC
  ROR
  TAY ;tile Y => Y

  TXA
  CLC
  ROR ;ABS(X/16) = tile X
  CLC
  ROR
  CLC
  ROR
  CLC
  ROR
  TAX ;tile X => X

  TYA
  CMP #$0F ;If tile Y is 15, it's not an actual tile (load 0 as replacement)
  BNE :+
  LDA #$00
  JMP EndLoadTileFromLocation
:

  TYA
  CLC
  ROL ;tile Y * 16 = tile y offset
  ROL
  ROL
  ROL
  STA temp

  TXA
  CLC
  ADC temp
  TAX
  LDA MapData2, X
  JMP EndLoadTileFromLocation


EndLoadTileFromLocation: ;Returns Tile in Accumulator
  ;LDA #$01
  RTS