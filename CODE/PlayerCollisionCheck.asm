
PlayerVertCollisionTest:


;;COLLISION CODE

  ;Reset Touching Ground Flag (and rest)
  LDA entities+Entity::data
  AND #PlayerData_BottomHit^$FF
  AND #PlayerData_TopHit^$FF
  AND #PlayerData_OffScreen^$FF
  STA entities+Entity::data




  ;BOTTOM (GROUND) COLLISION

  ;;Under Screen Collision
  LDA playerY+2
  AND #$80 ;Z clear if negative (if above screen, can't be under!)
  BNE SkipBottomScreenPlayerCollision
  LDA playerY+2
  CMP #$01 ;C set if (playerY+2) > 0
  BCS :+ ;If origin is past screen no need to test y pixel pos
  LDA playerY+1
  CLC
  ADC #$10 ;Account for lack of screen height
  ADC #$20 ;Add player height to player pos
  BCC SkipBottomScreenPlayerCollision ;If bottom player position doesn't wrap past screen, player is above screen bottom
:
;run this if player is below screen
  LDA entities+Entity::data
  AND #PlayerData_OffScreen^$FF
  CLC
  ADC #PlayerData_OffScreen
  STA entities+Entity::data

  JMP EndTopPlayerCollision


SkipBottomScreenPlayerCollision:


;If (the hitbox is) above screen, skip top collision, otherwise jumping over the screen collides with the bottom of the map
  LDA playerY+2
  AND #$80 ;Z set if negative (if below screen, can't be above!) NOTE: Negative is up
  BEQ SkipTopScreenPlayerCollision
  LDA playerY+1
  ;CMP $F0
  ;BCS SkipTopScreenPlayerCollision

  LDA entities+Entity::data
  AND #PlayerData_OffScreen^$FF
  CLC
  ADC #PlayerData_OffScreen
  STA entities+Entity::data

  JMP EndTopPlayerCollision


SkipTopScreenPlayerCollision:



;;USE TILES FOR COLLISION 
  ;Get tile under player
  LDA playerY+1
  CLC
  ADC #$20 ;Y+32PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$00 ;X+0PX
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Standing Flag
  LDA entities+Entity::data
  AND #PlayerData_BottomHit^$FF
  CLC
  ADC #PlayerData_BottomHit
  STA entities+Entity::data
:

  LDA playerY+1
  CLC
  ADC #$20 ;Y+32PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$0F ;X+15PX 
  BCS :+ ;Carry means load tile from next map
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  JMP :++
:
  LDA playerX+2
  LDX #$00
  AND #%00000001
  CMP #%00000001
  JSR LoadTileFromLocation
:

  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Standing Flag
  LDA entities+Entity::data
  AND #PlayerData_BottomHit^$FF
  CLC
  ADC #PlayerData_BottomHit
  STA entities+Entity::data
:

EndBottomPlayerCollision:


  ;IF touching ground
  LDA #PlayerData_BottomHit
  BIT entities+Entity::data
  BEQ :+

  ;Set player position on ground
  LDA playerY+1
  AND #%11110000
  STA playerY+1

  ;Set player Velocity
  LDA #$00
  STA playerVelY
  STA playerVelY+1
:



























;;TOP PLAYER COLLISION





  ;Get tile top of player
  LDA playerY+1
  CLC
  ADC #$00 ;Y+0PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$00 ;X+0PX
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Top Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_TopHit^$FF
  CLC
  ADC #PlayerData_TopHit
  STA entities+Entity::data
:

  LDA playerY+1
  CLC
  ADC #$00 ;Y+0PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$0F ;X+15PX 
  BCS :+ ;Carry means load tile from next map
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  JMP :++
:
  LDA playerX+2
  LDX #$00
  AND #%00000001
  CMP #%00000001
  JSR LoadTileFromLocation
:

  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Top Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_TopHit^$FF
  CLC
  ADC #PlayerData_TopHit
  STA entities+Entity::data
:

EndTopPlayerCollision:


  ;IF touching top
  LDA #PlayerData_TopHit
  BIT entities+Entity::data
  BEQ :+

  LDA #PlayerData_BottomHit
  BIT entities+Entity::data
  BNE :+

  ;Set player position
  LDA playerY+1
  AND #%11110000
  CLC
  ADC #$10
  STA playerY+1

  ;Set player Velocity
  LDA #$00
  STA playerVelY
  STA playerVelY+1
:


RTS











PlayerHorizCollisionTest:

  LDA entities+Entity::data
  AND #PlayerData_RightHit^$FF
  AND #PlayerData_LeftHit^$FF
  STA entities+Entity::data




;;If Off Screen Skip L+R Collision
  LDA #PlayerData_OffScreen
  BIT entities+Entity::data
  BEQ :+
  JMP EndLeftPlayerCollision
:







;;RIGHT PLAYER COLLISION
  ;Get tile right of player
  LDA playerY+1
  CLC
  ADC #$00 ;Y+0PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$10 ;X+16PX
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Right Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_RightHit^$FF
  CLC
  ADC #PlayerData_RightHit
  STA entities+Entity::data
:

  LDA playerY+1
  CLC
  ADC #$10 ;Y+16PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$10 ;X+16PX
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Right Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_RightHit^$FF
  CLC
  ADC #PlayerData_RightHit
  STA entities+Entity::data
:

  LDA playerY+1
  CLC
  ADC #$1E ;Y+31PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$10 ;X+16PX 
  BCS :+ ;Carry means load tile from next map
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  JMP :++
:
  LDA playerX+2
  LDX #$00
  AND #%00000001
  CMP #%00000001
  JSR LoadTileFromLocation
:

  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Right Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_RightHit^$FF
  CLC
  ADC #PlayerData_RightHit
  STA entities+Entity::data
:

EndRightPlayerCollision:

  ;IF touching right
  LDA #PlayerData_RightHit
  BIT entities+Entity::data
  BEQ :+

  ;Set player position
  LDA playerX+1
  AND #%11110000
  STA playerX+1

  ;Set player Velocity
  LDA #$00
  STA playerVelX
  STA playerVelX+1
:








;;LEFT PLAYER COLLISION
  ;Get tile left of player
  LDA playerY+1
  CLC
  ADC #$00 ;Y+0PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$00 ;X+0PX
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Left Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_LeftHit^$FF
  CLC
  ADC #PlayerData_LeftHit
  STA entities+Entity::data
:

  LDA playerY+1
  CLC
  ADC #$10 ;Y+16PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$00 ;X+0PX
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Left Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_LeftHit^$FF
  CLC
  ADC #PlayerData_LeftHit
  STA entities+Entity::data
:

  LDA playerY+1
  CLC
  ADC #$1E ;Y+31PX
  TAY
  LDX playerX+1
  TXA
  CLC
  ADC #$00 ;X+0PX 
  BCS :+ ;Carry means load tile from next map
  TAX
  LDA #%00000001
  BIT playerX+2
  JSR LoadTileFromLocation
  JMP :++
:
  LDA playerX+2
  LDX #$00
  AND #%00000001
  CMP #%00000001
  JSR LoadTileFromLocation
:

  TAX
  LDA MetatilePhysRef, X
  CMP #$00
  
  BEQ :+ ;If tile isn't empty
  ;Set Left Hit Flag
  LDA entities+Entity::data
  AND #PlayerData_LeftHit^$FF
  CLC
  ADC #PlayerData_LeftHit
  STA entities+Entity::data
:

EndLeftPlayerCollision:

  ;IF touching left
  LDA #PlayerData_LeftHit
  BIT entities+Entity::data
  BEQ :++

  ;Set player position
  LDA playerX+1
  AND #%11110000
  CLC
  ADC #$10
  BCC :+
  INC playerX+2
:
  STA playerX+1

  ;Set player Velocity
  LDA #$00
  STA playerVelX
  STA playerVelX+1
:



RTS