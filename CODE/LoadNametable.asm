LoadNametable:

;Load World
  ;Store World Data pointer to "world"
  LDX #$00
  LDA #<WorldData
  STA world
  LDA #>WorldData
  STA world+1

  ;setup PPU Address
  BIT PPUSTATUS ;Reset PPUADDR

;Point to Nametable $2000
  LDA #$20
  STA PPUADDR
  LDA #$00
  STA PPUADDR

  LDX #$00
  LDY #$00
LoadWorldLoop:
  LDA (world), Y
  STA PPUDATA
  INY
  ;if X==$03 and Y==$C0 goto DoneLoadingWorld
  CPX #$03
  BNE :+
  CPY #$C0 
  BEQ DoneLoadingWorld
:
  ;if Y==0, increment X and world high byte
  CPY #$00
  BNE LoadWorldLoop
  INX
  INC world+1
  JMP LoadWorldLoop ;Y Inc past FF, carry bit to X
DoneLoadingWorld:

  LDX #$00
SetAttributesNmt:
  LDA #$00
  STA PPUDATA
  INX
  CPX #$40
  BNE SetAttributesNmt

  RTS