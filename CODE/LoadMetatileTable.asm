





LoadFullMetatileTable:           ;;DO NOT LOAD FULL UNLESS NMI IS DISABLED!!!
  JSR LoadFullMetatileTablePrep
  JSR LoadAllMetatileColumns
  RTS

;Prerequisites
LoadFullMetatileTablePrep:

	;LDA #$00
	;STA PPUMASK ;Disable all PPU Rendering

;Load World
  ;Store World Data pointer to "world" 

LoadWorldPointer:
  LDA mapLoadX
  CLC
  ROL
  TAX
  LDA Level1MapPointers, X
  STA world
  LDA Level1MapPointers+1, X
  STA world+1









;;Load in full map data
  LDA #%00000001
  BIT mapLoadX
  BEQ :+



  ;Copy map data to MapData2
  LDY #$00 ;reset y to 0
MapData2CopyLoop:
  LDA (world), Y
  STA MapData2, Y
  INY
  BNE MapData2CopyLoop
  ;Set world High byte to MapData2
  LDA #$05
  STA world+1
  JMP :++
:
  ;Copy map data to MapData1
  LDY #$00
MapData1CopyLoop:
  LDA (world), Y
  STA MapData1, Y
  INY
  BNE MapData1CopyLoop
  ;Set world High byte to MapData1
  LDA #$04
  STA world+1
:









  ;Clear mapLoadTemp
  LDA #$00
  STA mapLoadTemp



;Set up Increment mode to +32
  JSR WaitVblank
  LDA ppuCtrlSet
  AND #%11111011
  CLC
  ADC #%00000100
  STA PPUCTRL
  STA ppuCtrlSet
  RTS











JMP :+ ;Skip test
;TEST SINGLE COLUMN LOAD
  LDY #$05
  ;setup PPU Address
  BIT PPUSTATUS ;Reset PPUADDR
;Point to Nametable PPUCTRL + tile offset
  LDA #$20
  STA PPUADDR
  TYA
  STA PPUADDR
  LDX #$05
  JSR LoadMetatileColumn
  JMP EndMetatileLoad
:



















LoadAllMetatileColumns:
  LDY #$00 ;Start with column 0

LoadAllMetatileColumnsLoop:
  ;setup PPU Address
  BIT PPUSTATUS ;Reset PPUADDR
;Point to Nametable PPUCTRL + tile offset

  LDA #%00000001
  BIT mapLoadX
  BEQ :+
  LDA #$24
  JMP :++
:
  LDA #$20 ;High byte is $20 + Nametable offset
:


  STA PPUADDR
  TYA
  STA PPUADDR
  CLC
  ROR
  TAX
  JSR LoadMetatileColumn
  INY
  CPY #$20
  BNE LoadAllMetatileColumnsLoop
  JMP EndMetatileLoad


LoadMetatileColumn:
;Load Map tile
  ;X parameter here is starting column (0-15)
  ;Y MUST BE EQUAL TO X
LoadMetatileColumnLoop:
  ;TODO Load actual tile

  TYA ;Put current column in A
  AND #%00000001 ;Test if odd column
  BEQ :+
  JMP LoadTopRightMetaspriteTile ;If odd column load right tiles
:
  ;else, load left tiles


  ;LOAD TOP LEFT TILE
LoadTopLeftMetaspriteTile:        ;NOTE:  Y IS COLUMN DATA     X IS TILE POSITION INDEX
  STX world ;Back up Tile Position Index in world
  LDA #$00
  STA mapLoadTemp ;Reset mapLoadTemp

  LDA #%00000001
  BIT mapLoadX ;Z flag set if even nametable
  BEQ :+
  LDA MapData2, X ;LDA Map data + Tile Index (Metatile Index)
  JMP :++
:
  LDA MapData1, X ;LDA Map data + Tile Index (Metatile Index)
:


  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
;  CLC
;  ADC #$00 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:



  STA PPUDATA ;Store Tile Top
  LDX world ;Reload Tile Position into X

  ;LOAD BOTTOM LEFT TILE
LoadBottomLeftMetaspriteTile:
  STX world ;Back up Tile Position Index in world
  LDA #$00
  STA mapLoadTemp ;Reset mapLoadTemp

  LDA #%00000001
  BIT mapLoadX ;Z flag set if even nametable
  BEQ :+
  LDA MapData2, X ;LDA Map data + Tile Index (Metatile Index)
  JMP :++
:
  LDA MapData1, X ;LDA Map data + Tile Index (Metatile Index)
:



  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
  CLC
  ADC #$01 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:



  STA PPUDATA ;Store Tile Bottom
  LDX world ;Reload Tile Position into X
  JMP FinishedMetaspriteTileLoad

  ;LOAD TOP Right TILE
LoadTopRightMetaspriteTile:
  STX world ;Back up Tile Position Index in world
  LDA #$00
  STA mapLoadTemp ;Reset mapLoadTemp

  LDA #%00000001
  BIT mapLoadX ;Z flag set if even nametable
  BEQ :+
  LDA MapData2, X ;LDA Map data + Tile Index (Metatile Index)
  JMP :++
:
  LDA MapData1, X ;LDA Map data + Tile Index (Metatile Index)
:



  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
  CLC
  ADC #$02 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:



  STA PPUDATA ;Store Tile Top
  LDX world ;Reload Tile Position into X

  ;LOAD BOTTOM Right TILE
LoadBottomRightMetaspriteTile:
  STX world ;Back up Tile Position Index in world
  LDA #$00
  STA mapLoadTemp ;Reset mapLoadTemp

  LDA #%00000001
  BIT mapLoadX ;Z flag set if even nametable
  BEQ :+
  LDA MapData2, X ;LDA Map data + Tile Index (Metatile Index)
  JMP :++
:
  LDA MapData1, X ;LDA Map data + Tile Index (Metatile Index)
:



  ;Multiply Metatile Index by 4
  CLC
  ROL
  BCC :+ ;If 7th bit is 1 add 2 pages
  INC mapLoadTemp
  INC mapLoadTemp
:
  CLC
  ROL
  BCC :+ ;if 6th bit is 1 add 1 page
  INC mapLoadTemp
:
;For non zero tiles
  CLC
  ADC #$03 ;Add offset for tile position

  TAX ;Store Metatile loading offset in X
  
  ;Determine How many pages to skip while loading
  LDA mapLoadTemp
  CMP #$01 ;If skipping one page
  BEQ :+
  CMP #$02 ;If skipping two pages
  BEQ :++
  CMP #$03 ;If skipping three pages
  BEQ :+++

  ;if skipping no pages
  LDA MetatileTileRef, X
  JMP :++++
:
  ;If skipping one page
  LDA MetatileTileRef+$100, X
  JMP :+++
:
  ;If skipping two pages
  LDA MetatileTileRef+$200, X
  JMP :++
:
  ;If skipping three pages
  LDA MetatileTileRef+$300, X
:



  STA PPUDATA ;Store Tile Bottom
  LDX world ;Reload Tile Position into X
  ;JMP FinishedMetaspriteTileLoad

FinishedMetaspriteTileLoad:



  TXA
  CLC
  ADC #$10 ;Increase Y position by 1
  CMP #$F0
  TAX
  BCS :+ 
  JMP LoadMetatileColumnLoop
:
  RTS

EndMetatileLoad:

























































;Set up Increment mode to +1
;  JSR WaitVblank
  LDA ppuCtrlSet
  AND #%11111011
  STA PPUCTRL
  STA ppuCtrlSet






;Point to Nametable $23C0 + nametable offset, Attributes
  LDA #%00000001
  BIT mapLoadX ;Z flag set if even nametable
  BEQ :+
  LDA #$27
  JMP :++
:
  LDA #$23 ;High byte is $20 + Nametable offset
:
  ;setup PPU Address
  BIT PPUSTATUS ;Reset PPUADDR
  STA PPUADDR
  LDA #$C0
  ;Add offset here
  STA PPUADDR


  LDA #$00 ;Reset world pointer low byte ($0400 or $0500)
  STA world

  LDA #$00 ;Reset mapLoadTemp (Use to store ATTR)
  STA mapLoadTemp















  LDX #$00
;:SET TILE ATTRIBUTES
SetAttributes:

  LDA #$00
  STA mapLoadTemp

  ;ADD BOTTOM RIGHT COLOR
  ;Vertical offset
  TXA
  AND #%11111000
  CLC
  ROL
  ROL ;Skip 32 tiles for each 8+ place bits
  STA temp

  ;Horizontal offset
  TXA ;Attr Index to X
  AND #%00000111
  CLC
  ROL ;Attr Index * 2
  ADC #$11 ;Point to bottom right metatile
  ADC temp
  TAY
  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL ;Move 2 bits left
  STA mapLoadTemp




  ;ADD BOTTOM LEFT COLOR
  ;Vertical offset
  TXA
  AND #%11111000
  CLC
  ROL
  ROL ;Skip 32 tiles for each 8+ place bits
  STA temp

  ;Horizontal offset
  TXA ;Attr Index to X
  AND #%00000111
  CLC
  ROL ;Attr Index * 2
  ADC #$10 ;Point to bottom right metatile
  ADC temp
  TAY
  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL ;Move 2 bits left
  STA mapLoadTemp

  


  ;ADD TOP RIGHT COLOR
  ;Vertical offset
  TXA
  AND #%11111000
  CLC
  ROL
  ROL ;Skip 32 tiles for each 8+ place bits
  STA temp

  ;Horizontal offset
  TXA ;Attr Index to X
  AND #%00000111
  CLC
  ROL ;Attr Index * 2
  ADC #$01 ;Point to bottom right metatile
  ADC temp
  TAY
  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL ;Move 2 bits left
  STA mapLoadTemp




  ;ADD TOP LEFT COLOR
  ;Vertical offset
  TXA
  AND #%11111000
  CLC
  ROL
  ROL ;Skip 32 tiles for each 8+ place bits
  STA temp

  ;Horizontal offset
  TXA ;Attr Index to X
  AND #%00000111
  CLC
  ROL ;Attr Index * 2
  ADC #$00 ;Point to bottom right metatile
  ADC temp
  TAY





  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  STA mapLoadTemp















;Store ATTR byte to PPU
  STA PPUDATA
  INX
  CPX #$38
  ;BNE SetAttributes
  BEQ :+
  JMP SetAttributes
:

SetLastAttributes:


  LDA #$00
  STA mapLoadTemp

  ;ADD TOP RIGHT COLOR
  ;Vertical offset
  TXA
  AND #%11111000
  CLC
  ROL
  ROL ;Skip 32 tiles for each 8+ place bits
  STA temp

  ;Horizontal offset
  TXA ;Attr Index to X
  AND #%00000111
  CLC
  ROL ;Attr Index * 2
  ADC #$01 ;Point to bottom right metatile
  ADC temp
  TAY
  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  ROL
  ROL ;Move 2 bits left
  STA mapLoadTemp




  ;ADD TOP LEFT COLOR
  ;Vertical offset
  TXA
  AND #%11111000
  CLC
  ROL
  ROL ;Skip 32 tiles for each 8+ place bits
  STA temp

  ;Horizontal offset
  TXA ;Attr Index to X
  AND #%00000111
  CLC
  ROL ;Attr Index * 2
  ADC #$00 ;Point to bottom right metatile
  ADC temp
  TAY





  ;LOAD TILE COLOR AND ADD TO ATTR
  LDA (world),Y ;Load Tile Index
  TAY
  LDA MetatileAttrRef, Y ;Load Color Pal
  CLC
  ADC mapLoadTemp ;Add to ATTR
  STA mapLoadTemp















;Store ATTR byte to PPU
  STA PPUDATA
  INX
  CPX #$40
  ;BNE SetAttributes
  BEQ :+
  JMP SetLastAttributes
:





  RTS