Reset:
  SEI ; Disables all interrupts
  CLD ; disable decimal mode

  ; Disable sound IRQ
  LDX #$40
  STX $4017

  ; Initialize the stack register
  LDX #$FF
  TXS


  ; Zero out the PPU registers
  JSR WaitVblank
  LDX #$00
  STX PPUCTRL
  STX ppuCtrlSet
  STX $2001

  ;Disable PCM Sound
  STX $4010


;Wait for V-Blank
  LDA #%10000000 ;;Mask for BIT (7th bit for VBLANK)
:
  BIT PPUSTATUS ;;Test Bit 7
  BPL :-




  
  LDX #$00
  TXA ;; #$00 => A
;Clear Memory Loop
ClearMem:
  STA $0000, X ; $0000 => $00FF
  STA $0100, X ; $0100 => $01FF
  STA $0300, X ; $0300 => $03FF
  STA $0400, X
  STA $0500, X
  STA $0600, X
  STA $0700, X
  LDA #$FF
  STA $0200, X ; $0200 => $02FF
  LDA #$00
  INX
  BNE ClearMem   


;Wait for vblank
  LDA #%10000000 ;;Mask for BIT (7th bit for VBLANK)
:
  BIT PPUSTATUS ;;Test Bit 7
  BPL :-

;;Tell Sprite OAM to take from page $0200-$02FF
  LDA #$02
  STA $4014
  NOP

  ;Start Writing to Palette (PPU $3F00)
  LDA #$3F
  STA PPUADDR
  LDA #$00
  STA PPUADDR

  LDX #$00

LoadPalettes:
  LDA PaletteData, X
  STA PPUDATA
  INX
  CPX #$20
  BNE LoadPalettes






  


;Test load map into both nametables
  LDA #$00
  STA mapLoadX
  JSR LoadFullMetatileTable

  LDA #$01
  STA mapLoadX
  JSR LoadFullMetatileTable



;;Disable column loading by default
  LDA #$00
  STA mapLoadTemp

  LDA #$00
  STA mapLoadColumn









;Set Color and Tile stacks as empty
LDA #$FF
STA tileStackPointer
STA colorStackPointer









;Create Player Entity
  LDA #$00
  STA entities+Entity::xpos
  STA entities+Entity::ypos
  STA entities+Entity::data
  LDA #EntityType::PlayerType
  STA entities+Entity::type

  LDA #$20
  STA playerX+1
  LDA #$B0
  STA playerY+1


;Create Second Player Entity (TEST ONLY)
  LDA #$50
  STA entities+Entity::xpos+.sizeof(Entity)
  LDA #$40
  STA entities+Entity::ypos+.sizeof(Entity)
  LDA #EntityType::PlayerType
  STA entities+Entity::type+.sizeof(Entity)
  LDA #$00
  STA entities+Entity::data+.sizeof(Entity)
  STA entities+Entity::yposbig+.sizeof(Entity)
  LDA #$01
  STA entities+Entity::xposbig+.sizeof(Entity)



;;Test load block into tile stack
;LDA #$22
;STA 
























  ;Set fine scroll Pos
  BIT PPUSTATUS
  LDA #$00
  STA PPUSCROLL
  STA PPUSCROLL

;Enable Interrupts
  JSR WaitVblank
  CLI
  LDA #%10010000 ;Enable NMI and set BG tiles to 2nd set
  STA PPUCTRL
  STA ppuCtrlSet
  LDA #%00011000 ;Enable Sprites and BG for left 8 pixels, Enable Sprites and BG
  STA PPUMASK

  LDA #%00000001 ;Store Initial gameState, GSF Flag (1 Finished)
  STA gameStatus

  JMP GameLoop
