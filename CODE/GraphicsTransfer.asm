;Address Labels
graphicsEntities = $0300



;;Copy Entity data to graphicsEntities
LDX #$00
CopyEntityGrLoop:
LDA entities, X
STA graphicsEntities, X
INX
CPX #TOTALENTITIES
BNE CopyEntityGrLoop

















































;;DRAW ENTITIES

;Reset Registers
  LDX #$00
  LDY #$00

DrawEntities:
  ;Reset sprite Pointer and entity index / pointer
  LDA #$00

  STA entityIndex
  STA entityPointer
  LDA #$01 ;SAVE SPRITE ZERO
  STA spriteIndex
  LDA #$04
  STA spritePointer

DrawEntitiesLoop:
  ;;Branch to correct entity type
  LDX entityPointer
  LDA graphicsEntities+Entity::type, X
  CMP #EntityType::PlayerType
  BEQ DrawRabbit
  JMP DrawEntitiesCheck
  

















DrawRabbit:

  ;Store sprite pointer

  LDA #PlayerData_BottomHit
  BIT entities+Entity::data
  BEQ :+
  JMP DrawRabbitOnGround
:


  ;Draw Rabbit Air
  LDX #$00
  LDA #$80
  BIT playerVelY+1
  BNE :+
  LDX #$02
:
  JMP EndDrawRabbit

DrawRabbitOnGround:
  LDA #PlayerData_Walking
  BIT entities+Entity::data
  BEQ :+
  JMP DrawRabbitWalking
:

DrawRabbitStanding:
  LDA globalTimer
  AND #%00011000
  CMP #%00000000
  BNE :+
  LDX #$0A
  JMP EndDrawRabbitStanding
:
  CMP #%00001000
  BNE :+
  LDX #$0C
  JMP EndDrawRabbitStanding
:
  CMP #%00010000
  BNE :+
  LDX #$0E
  JMP EndDrawRabbitStanding
:
  LDX #$0C
  JMP EndDrawRabbitStanding


EndDrawRabbitStanding:
  JMP EndDrawRabbit

DrawRabbitWalking:
  LDA globalTimer
  AND #%00011000
  CMP #%00000000
  BNE :+
  LDX #$04
  JMP EndDrawRabbitWalking
:
  CMP #%00001000
  BNE :+
  LDX #$6
  JMP EndDrawRabbitWalking
:
  CMP #%00010000
  BNE :+
  LDX #$08
  JMP EndDrawRabbitWalking
:
  LDX #$06
  JMP EndDrawRabbitWalking
EndDrawRabbitWalking:



  JMP EndDrawRabbit



EndDrawRabbit:
  LDA #PlayerData_LastDirection
  BIT entities+Entity::data

  BEQ :+
  TXA
  CLC
  ADC #$10
  TAX
:
  LDA rabbit_pointers, X
  STA metaspritePointer
  LDA rabbit_pointers+1, X
  STA metaspritePointer+1
  JMP DrawSingleEntity




















































;;DRAW EACH ENTITY FROM POINTER
DrawSingleEntity:
  LDA #$00
  STA temp ;Use Temp to store flags while drawing entities









LDY #$00
DrawEntityLoop:  

  LDA temp
  AND #DrawEntityF_SideWrap^$FF
  STA temp ;Use Temp to store flags while drawing entities


  ;STORE SPRITE X DATA in $0203+spritePointer
  
  LDA (metaspritePointer), Y
  CMP #128
  BNE :+
  JMP DrawEntitiesCheck
:
  LDX entityPointer
  CLC
  ADC graphicsEntities+Entity::xpos, X
  STA temp
  LDA graphicsEntities+Entity::xposbig, X
  ADC #$00
  STA temp+1
;Store world X position of sprite in temp +0-1



  LDA (metaspritePointer), Y
  LDX entityPointer
  CLC
  ADC graphicsEntities+Entity::xpos, X
  SEC
  SBC cameraX
  LDX spritePointer
  STA $0203, X




  ;STORE SPRITE Y DATA in $0200+spritePointer
  INY

  LDA (metaspritePointer), Y
  LDX entityPointer
  CLC
  ADC graphicsEntities+Entity::ypos, X
  STA temp+2
  LDA graphicsEntities+Entity::yposbig, X
  ADC #$00
  STA temp+3



  LDA (metaspritePointer), Y
  LDX entityPointer
  CLC
  ADC graphicsEntities+Entity::ypos, X
  LDX spritePointer
  STA $0200, X


  ;STORE SPRITE ATTR DATA in $0201+spritePointer
  INY
  LDA (metaspritePointer), Y
  STA $0201, X


  ;STORE SPRITE TILE DATA in $0202+spritePointer
  INY
  LDA (metaspritePointer), Y
  STA $0202, X
  INY



;Debug laod temp
LDA temp
STA $FF








;Skip This Sprite
;  LDA #$FF
;  STA $0201, X
;  JMP DrawEntityLoop ;If not sprite end go back to loop


;SKIP SPRTIE IF OFF SCREEN


;;IF (Sprite.X < Camera.X) Don't Render
LDA temp+1
CMP cameraX+1
BCS :+
JMP SkipSpriteRender ;Don't Render This Sprite
:
BEQ :+
JMP EndLeftSpriteCheck
:
LDA temp
CMP cameraX
BCS :+
JMP SkipSpriteRender ;Don't Render This Sprite
:
JMP EndLeftSpriteCheck
EndLeftSpriteCheck:

;;IF (Sprite.X >= Camera.X+256) Don't Render
LDA temp+1
INC cameraX+1
CMP cameraX+1
BCS :+
;JMP SkipSpriteRender ;Don't Render This Sprite
DEC cameraX+1
JMP EndRightSpriteCheck
:
BEQ :+
;JMP EndRightSpriteCheck
DEC cameraX+1
JMP SkipSpriteRender
:
LDA temp
CMP cameraX
BCS :+
;JMP SkipSpriteRender ;Don't Render This Sprite
DEC cameraX+1
JMP EndRightSpriteCheck
:
;JMP EndRightSpriteCheck
DEC cameraX+1
JMP SkipSpriteRender
EndRightSpriteCheck:

LDA temp+3
BEQ :+
JMP SkipSpriteRender
:

LDA temp+2
CMP #$F0
BCC :+
JMP SkipSpriteRender
:















JMP DontSkipSpriteRender
SkipSpriteRender:

  LDA #$FF
  STA $0201, X
  JMP DrawEntityLoop ;If not sprite end go back to loop

DontSkipSpriteRender:




















  ;Increase sprite pointer and index
  INC spritePointer
  INC spritePointer
  INC spritePointer
  INC spritePointer
  INC spriteIndex




  ;;Check sprite overflow
  LDA spriteIndex
  CMP #$40
  BNE :+
  JMP DrawEntitiesEnd ;Stops drawing any more sprites if excedes 64 sprites
:

  JMP DrawEntityLoop 



EndDrawEntity:
  JMP DrawEntitiesCheck














;;;CHECK IF THERE ARE MORE ENTITIES TO DRAW
DrawEntitiesCheck:
  INC entityIndex
  
  LDA entityPointer
  CLC
  ADC #.sizeof(Entity)
  STA entityPointer

  
  ;;Check if finished
  LDA entityIndex
  CMP #MAXENTITIES
  BEQ :+
  JMP DrawEntitiesLoop
:

;;If finished fill blank sprites
  LDA #$FF
  LDX spritePointer
DrawEmptySprites:
  STA $0200, X
  INX
  CPX #$00
  BNE DrawEmptySprites
DrawEntitiesEnd:








  
;;See end of Graphics Transfer in Mesen event viewer (Ctrl + E)
  BIT PPUSTATUS