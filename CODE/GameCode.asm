
  JSR ReadJoy ;GET CONTROLLER INPUT



;Increment Global Timer
  INC globalTimer
  BNE :+
  INC globalTimer+1
:





;;SET CAMERA BOUNDS
  LDA #$00
  STA screenBoundLeft

  LDA #$04
  STA screenBoundRight



































;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;
;             P L A Y E R      C O D E
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;Apply Gravity
  LDA playerVelY
  CLC
  ADC #$80 ;Low Bit
  STA playerVelY
  LDA playerVelY+1
  ADC #$00 ;High Bit
  STA playerVelY+1


;;WALKING/RUNNING LEFT AND RIGHT


  ;Reset player Walking flag
  LDA entities+Entity::data
  AND #PlayerData_Walking^$FF
  STA entities+Entity::data


  ;If Holding Right set X velocity
  LDA #%00000001
  BIT buttons1
  BEQ RightInputEnd



  ;Set player Walking flag
  LDA entities+Entity::data
  AND #PlayerData_Walking^$FF
  CLC
  ADC #PlayerData_Walking
  STA entities+Entity::data


  ;Set Last Direction

  LDA entities+Entity::data
  AND #PlayerData_LastDirection^$FF
  STA entities+Entity::data


  LDA #%01000000
  BIT buttons1
  BNE AddRightInputVelocityRunning

  ;;Increase Velocity
AddRightInputVelocityWalking:
  LDA playerVelX 
  CLC
  ADC #$10 ;Low Bit
  STA playerVelX
  LDA playerVelX+1
  ADC #$00 ;High Bit
  STA playerVelX+1
  ;Cap Right Velocity
  CMP #$80 ;If velocity is negative, skip cap
  BCS RightInputEnd
  CMP #$02 ;If Velocity > 4 set velocity to cap
  BCC RightInputEnd
  ;Set Velocity cap
  ;LDA #$00
  ;STA playerVelX
  ;LDA #$02
  ;STA playerVelX+1
  JMP RightInputEnd
  AddRightInputVelocityRunning:
  LDA playerVelX 
  CLC
  ADC #$20 ;Low Bit
  STA playerVelX
  LDA playerVelX+1
  ADC #$00 ;High Bit
  STA playerVelX+1
  ;Cap Right Velocity
  CMP #$80 ;If velocity is negative, skip cap
  BCS RightInputEnd
  CMP #$06 ;If Velocity > 4 set velocity to cap
  BCC RightInputEnd
  ;Set Velocity cap
  LDA #$00
  STA playerVelX
  LDA #$06
  STA playerVelX+1
  JMP RightInputEnd

RightInputEnd:

  ;If Holding Left set X velocity
  LDA #%00000010
  BIT buttons1
  BEQ LeftInputEnd

  ;Set player Walking flag
  LDA entities+Entity::data
  AND #PlayerData_Walking^$FF
  CLC
  ADC #PlayerData_Walking
  STA entities+Entity::data

  ;Set Last Direction

  LDA entities+Entity::data
  AND #PlayerData_LastDirection^$FF
  CLC
  ADC #PlayerData_LastDirection
  STA entities+Entity::data



  LDA #%01000000
  BIT buttons1
  BNE AddLeftInputVelocityRunning


AddLeftInputVelocityWalking:
  LDA playerVelX
  CLC
  ADC #$F0 ;Low Bit
  STA playerVelX
  LDA playerVelX+1
  ADC #$FF ;High Bit
  STA playerVelX+1
  ;Cap Left Velocity
  CMP #$80 ;If velocity is positive, skip cap
  BCC LeftInputEnd
  CMP #$FE ;If Velocity < -4 set velocity to cap
  BCS LeftInputEnd
  ;Set Velocity cap
  ;LDA #$00
  ;STA playerVelX
  ;LDA #$FE
  ;STA playerVelX+1
  JMP LeftInputEnd
AddLeftInputVelocityRunning:
  LDA playerVelX
  CLC
  ADC #$E0 ;Low Bit
  STA playerVelX
  LDA playerVelX+1
  ADC #$FF ;High Bit
  STA playerVelX+1
  ;Cap Left Velocity
  CMP #$80 ;If velocity is positive, skip cap
  BCC LeftInputEnd
  CMP #$FA ;If Velocity < -6 set velocity to cap
  BCS LeftInputEnd
  ;Set Velocity cap
  LDA #$00
  STA playerVelX
  LDA #$FA
  STA playerVelX+1
  JMP LeftInputEnd

LeftInputEnd:















;;PLAYER FRICTION
  LDA #%00000011 ;If directional buttons not held
  BIT buttons1
  BEQ ApplyPlayerFriction ;Apply Friction
  
  ;OR

  LDA #%01000000 ;If NOT Running
  BIT buttons1
  BNE :++ ;Player is running

  LDA playerVelX+1
  CMP #$80
  BCS :+ ;And if Friction is negative
;;POSITIVE WALK SLOWDOWN CHECK
  LDA playerVelX+1
  CMP #$02 ;If speed is over positive cap, apply friction despite walking
  BCS ApplyPlayerFriction
  JMP EndPlayerFriction
;;NEGATIVE WALK SLOWDOWN CHECK
:
  LDA playerVelX+1
  CMP #$FE
  BCC ApplyPlayerFriction
  JMP EndPlayerFriction
:
  JMP EndPlayerFriction

ApplyPlayerFriction:
;;Check if positive
  LDA playerVelX+1
  AND #$FF
  BMI PlayerFrictionNeg

PlayerFrictionPos:
  LDA playerVelX
  CLC
  ADC #$C0 ;Low Bit
  STA playerVelX
  LDA playerVelX+1
  ADC #$FF ;High Bit
  STA playerVelX+1
  ;Cap Left Velocity
  ;LDA playerVelX+1
  CMP #$80 ;If Zero Velocity passed, stop
  BCC EndPlayerFriction
  LDA #$00
  STA playerVelX
  LDA #$00
  STA playerVelX+1
  JMP EndPlayerFriction

PlayerFrictionNeg:
  LDA playerVelX
  CLC
  ADC #$40 ;Low Bit
  STA playerVelX
  LDA playerVelX+1
  ADC #$00 ;High Bit
  STA playerVelX+1
  ;Cap Left Velocity
  ;LDA playerVelX+1
  CMP #$80 ;If Zero Velocity passed, stop
  BCS :+
  LDA #$00
  STA playerVelX
  LDA #$00
  STA playerVelX+1
:
  JMP EndPlayerFriction
EndPlayerFriction:
















;;JUMP INPUT

  ;If Jump Pressed Set Y Velocity to -7.0
  LDA #%10000000 ;If Button just pressed
  BIT buttons1
  BEQ :+
  LDA #%10000000
  BIT buttons1old
  BNE :+


  LDA #%10000000 ;If touching ground
  BIT entities+Entity::data
  BEQ :+


  LDA #$80
  STA playerVelY
  LDA #$F7
  STA playerVelY+1
:















;;Apply Velocity
  ;Apply Player X Velocity

  LDA playerX
  CLC
  ADC playerVelX
  STA playerX
  LDA playerX+1
  ADC playerVelX+1
  STA playerX+1

  LDA playerVelX+1
  AND #%10000000 ;Check Sign
  BNE :+
  LDA playerX+2
  ADC #$00
  STA playerX+2
  JMP EndApplyPlayerXVelocity
:
  BCS :+
  DEC playerX+2
:
JMP EndApplyPlayerXVelocity
ResetXVelocityIfWallHit:
LDA #$00
STA playerVelX
STA playerVelX+1

EndApplyPlayerXVelocity:

JSR PlayerHorizCollisionTest




  ;Apply Player Y Velocity

  LDA playerY
  CLC
  ADC playerVelY
  STA playerY
  LDA playerY+1
  ADC playerVelY+1
  STA playerY+1

  LDA playerVelY+1
  AND #%10000000 ;Check Sign
  BNE :+
  LDA playerY+2
  ADC #$00
  STA playerY+2
  JMP EndApplyPlayerYVelocity
:
  BCS :+
  DEC playerY+2
:

EndApplyPlayerYVelocity:

JSR PlayerVertCollisionTest




































;;KEEP PLAYER POSITION IN CAMERA BOUNDS


;If on camera border screen, keep towards left
  LDA playerX+2
  CMP screenBoundLeft
  BNE :+

  LDA playerX+1
  CMP #$08
  BCS :+

  LDA #$00
  STA playerX
  STA playerVelX
  STA playerVelX+1
  LDA screenBoundLeft
  STA playerX+2
  LDA #$08
  STA playerX+1
:

;If on camera border screen, keep towards right
  DEC screenBoundRight
  LDA playerX+2
  CMP screenBoundRight
  BNE :+

  LDA playerX+1
  CMP #$E8
  BCC :+

  LDA #$00
  STA playerX
  STA playerVelX
  STA playerVelX+1
  LDA screenBoundRight
  STA playerX+2
  LDA #$E8
  STA playerX+1
:
  INC screenBoundRight








  ;Store Player Pos in Player Entity
  LDA playerX+1
  SEC
  SBC #$08
  STA entities+Entity::xpos
  LDA playerX+2
  SBC #$00
  STA entities+Entity::xposbig
  LDA playerY+1
  SEC
  SBC #$10
  STA entities+Entity::ypos
  LDA playerY+2
  STA entities+Entity::yposbig





















;;CAMERA CODE

;Store old camera position
  LDA cameraX
  STA cameraXold
  LDA cameraX+1
  STA cameraXold+1




;Set Camera Behind Mario
  LDA playerX+1
  SEC
  SBC #$80
  STA cameraX
  LDA playerX+2
  SBC #$00
  STA cameraX+1



  JMP EndAddVelocityToCamera
;OFFSET CAMERA BY VELOCITY
  LDA playerVelX
  STA temp
  LDA playerVelX+1
  STA temp+1 ;Store copy of velocity in temp

  LDX #$00

  LDA playerVelX+1 ;Do different process if negative
  AND #%10000000
  CMP #$00 ;Zero flag if Positive
  BEQ :+
  JMP AddNegativeVelocityToCamera
:



AddPositiveVelocityToCamera:
:
  LDA temp
  CLC
  ROL ;Multiply Velocity to get greater effect
  STA temp
  LDA temp+1
  ROL
  STA temp+1
  INX
  CPX #$02
  BNE :-




  LDA temp+1
  CLC
  ADC cameraX
  STA cameraX
  LDA cameraX+1
  ADC #$00
  STA cameraX+1



  JMP EndAddVelocityToCamera
AddNegativeVelocityToCamera:
  LDA temp ;get ABS(temp)
  EOR #$FF
  STA temp
  LDA temp+1
  EOR #%11111111
  STA temp+1
  LDA temp
  CLC
  ADC #$01
  STA temp
  LDA temp+1
  ADC #$00
  STA temp+1

:
  LDA temp
  CLC
  ROL ;Multiply Velocity to get greater effect
  STA temp
  LDA temp+1
  ROL
  STA temp+1
  INX
  CPX #$02
  BNE :-



  LDA cameraX
  SEC
  SBC temp+1
  STA cameraX
  LDA cameraX+1
  SBC #$00
  STA cameraX+1


  LDA buttons1
  STA buttons1old
  JMP EndAddVelocityToCamera

EndAddVelocityToCamera:






;;Camera Lead Room

;;Skip camera lead (disabled)
JMP SkipCameraLeadRoom





;;UNUSED (maybe return to later?)
  ;If No Movement Don't change lead room offset
  LDA playerVelX
  BNE :+
  LDA playerVelX+1
  BNE :+
  JMP SkipCameraLeadRoom
:

  ;LDA #$80
  ;BIT playerVelX+1 ;Check sign (Negative means left)
  LDA #%00000001 ;Right Button Held
  BIT buttons1
  BNE :+
  JMP CameraLeadLeft
:


CameraLeadRight: ;Prepare to load map data on a column right to the screen
  LDA cameraLead
  CLC
  ADC #$02
  CMP #$80
  BCC :+
  LDA #$80
:
  STA cameraLead
  JMP SkipCameraLeadRoom

CameraLeadLeft: ;Prepare to load map data on a column left to the screen
  LDA cameraLead
  SEC
  SBC #$02
  CMP #$E0
  BCC :+
  LDA #$00
:
  STA cameraLead


SkipCameraLeadRoom:

;Apply Camera Lead Room
LDA cameraX
CLC
ADC cameraLead
STA cameraX
LDA cameraX+1
ADC #$00
STA cameraX+1



















  ;;Left Bound Limit
  LDA screenBoundLeft
  BNE ScreenBoundLeftNonZero

ScreenBoundLeftZero:
  LDA cameraX+1
  CMP #$FF
  BNE :+
  LDA #$00
  STA cameraX
  STA cameraX+1
:
  JMP EndScreenBoundLeft


ScreenBoundLeftNonZero:

;CheckNegative
  LDA screenBoundLeft
  CMP #$80
  BCS ScreenBoundLeftNonZeroNegative

  LDA cameraX+1
  CMP screenBoundLeft
  BCS :+
  LDA #$00
  STA cameraX
  LDA screenBoundLeft
  STA cameraX+1
:
  JMP EndScreenBoundLeft
ScreenBoundLeftNonZeroNegative:
  ;If camera is positive, we know it's not past the bound
  LDA cameraX+1
  CMP #$80
  BCC EndScreenBoundLeft

  LDA cameraX+1
  CMP screenBoundLeft
  BCS :+
  LDA #$00
  STA cameraX
  LDA screenBoundLeft
  STA cameraX+1
:


EndScreenBoundLeft:








  ;;Right Bound Limit
  LDA screenBoundRight
  BNE ScreenBoundRightNonZero

ScreenBoundRightZero:
  LDA cameraX+1
  CMP #$FF
  BNE :+
  LDA #$00
  STA cameraX
  LDA #$FF
  STA cameraX+1
:
  JMP EndScreenBoundRight


ScreenBoundRightNonZero:

;CheckNegative
  LDA screenBoundRight
  CMP #$80
  BCS ScreenBoundRightNonZeroNegative

  LDA cameraX+1
  DEC screenBoundRight
  CMP screenBoundRight
  BNE :+
  LDA #$00
  STA cameraX
  LDA screenBoundRight
  STA cameraX+1
:
  INC screenBoundRight
  JMP EndScreenBoundRight
ScreenBoundRightNonZeroNegative:
  DEC screenBoundRight
  LDA cameraX+1
  CMP screenBoundRight
  BNE :+
  LDA #$00
  STA cameraX
  LDA screenBoundRight
  STA cameraX+1
:
  INC screenBoundRight

EndScreenBoundRight:

















  ;;Calc Difference Between New Camera Pos and Old Camera Pos (for column loading)
  LDA cameraX
  SEC
  SBC cameraXold
  STA cameraXvel
  LDA cameraX+1
  SBC cameraXold+1
  STA cameraXvel+1



  ;;If No Movement Don't load columns
  LDA #$00
  STA mapLoadTemp
  LDA cameraXvel
  BNE :+
  LDA cameraXvel+1
  BNE :+
  JMP SkipColumnPrep
:

  LDA #$FF
  STA mapLoadTemp

  LDA #$80
  BIT cameraXvel+1 ;Check sign (Negative means left)
  BEQ :+
  JMP ColumnPrepLeft
:


ColumnPrepRight: ;Prepare to load map data on a column right to the screen
  LDA cameraX+1
  CLC
  ADC #$01
  STA mapLoadX
  LDA cameraX
  CLC
  ROR
  CLC
  ROR
  CLC
  ROR
  STA mapLoadColumn
  JMP SkipColumnPrep

ColumnPrepLeft: ;Prepare to load map data on a column left to the screen
  LDA cameraX+1
  STA mapLoadX
  LDA cameraX
  CLC
  ROR
  CLC
  ROR
  CLC
  ROR
  STA mapLoadColumn


SkipColumnPrep:
























  ;;Transfer Graphics for next frame
  .include "CODE/GraphicsTransfer.asm"

  ;;Test proc loading all tiles
  ;LDA #$FF
  ;STA mapLoadTemp

  ;;Test proc loading all tiles
  ;INC mapLoadColumn
  ;LDA mapLoadColumn
  ;CMP #$20
  ;BNE :+
  ;LDA #$00
  ;STA mapLoadColumn
  ;INC mapLoadX
;:


;;Load nametable column
  LDA mapLoadTemp
  CMP #$FF
  BEQ :+
  JMP SkipLoadColumn
:
  LDY mapLoadColumn
  .include "CODE/LoadMetatileColumn.asm"
SkipLoadColumn:


