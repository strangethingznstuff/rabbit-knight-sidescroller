;;_________________________________________
;;             I-NES HEADER
;;(Describes the features of the cartridge)
;;-----------------------------------------
.segment "HEADER"

INES_MAPPER = %00000010 ; 0 = UNROM
INES_MIRROR = %00000001 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = %00000000 ; 1 = battery backed SRAM at $6000-7FFF


.byte "NES", $1a
.byte 16 ; 16 * 16KB PRG ROM
.byte 1 ; 0 * 8KB CHR ROM (UxROM)
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4) ;(Flags 6 & 7) mapper and mirroring
.byte (INES_MAPPER & %11110000)                                  ; ^
.byte %00000000                                                  ;(Flags 8) PRG RAM size (8kb measurements)
.byte %00000000                                                  ;(Flags 9) 
.byte %00100000                                                  ;(Flags 10) 
;        ||  ||
;        ||  ++- TV system (0: NTSC; 2: PAL; 1/3: dual compatible)
;        |+----- PRG RAM ($6000-$7FFF) (0: present; 1: not present)
;        +------ 0: Board has no bus conflicts; 1: Board has bus conflicts
.byte $00, $00, $00, $00, $00 ; filler bytes

;;_________________________________________
;;         Scopes N' Structs
;;
;;-----------------------------------------

;Entity Type
.scope EntityType
  NoEntity = 0
  PlayerType = 1
  Goomba = 2
.endscope

;Entity Struct
.struct Entity
  xpos    .byte
  xposbig .byte
  ypos    .byte
  yposbig .byte
  type    .byte
  data    .byte
.endstruct

.struct BigTile
  addressHigh .byte ;High Byte of top tile in PPU
  addressLow  .byte ;Low Byte of top tile in PPU
  tile0       .byte ;Pattern to load for top tile
  tile1       .byte ;Pattern to load for bottom tile
.endstruct

.struct NametableColor
  addressHigh .byte ;High Byte of top tile in PPU
  addressLow  .byte ;Low Byte of top tile in PPU
  color       .byte ;Attribute to be loaded
.endstruct



;;_________________________________________
;;         ZEROPAGE LABELS
;;(Defining labels in the zeropage of RAM)
;;-----------------------------------------
.segment "ZEROPAGE" ;FROM $0002-$00DF ($00E0-00FF Reserved for color palette)
;BANK
currentBank: .res 1 ;Current ROM bank
;CONTROLS
buttons1: .res 1
buttons1old: .res 1
;Entities
MAXENTITIES = 10
entities: .res .sizeof(Entity) * MAXENTITIES
TOTALENTITIES = .sizeof(Entity) * MAXENTITIES

;Misc
temp: .res 4


;Game State
gameStatus: .res 1 ;Game Status
;Bit 0 - (GSF) Game State Finished Flag
;Bit 1 -
;Bit 2 -
;Bit 3 - 
;Bit 4 -
;Bit 5 - 
;Bit 6 - 
;Bit 7 -

;Timers
globalTimer: .res 2



;Player
playerX: .res 3 ; Player X Position - 2^-8 2^0 2^8
playerY: .res 3 ; Player Y Position - 2^-8 2^0
playerVelX: .res 2 ; Player X Velocity (Signed -128) - 2^-8 2^0
playerVelY: .res 2 ; Player Y Velocity (Signed -128) - 2^-8 2^0
;playerWidth: .res 1
;playerLength: .res 1



;Map Loading
mapLoadTemp: .res 1 ;Temporary variable used while loading maps
mapLoadX: .res 1 ;X position in world (Nametable width unit 256px ea) of the map loaded
mapLoadColumn: .res 1
;mapLoadIndex: .res 1  
;;^ Idea is to eventually use pointers to load level data 
;;(eg, load map index 04 -> map 04 address => world -> map data => RAM -> load metatiles)




;Graphics State
ppuCtrlSet: .res 1 ;Save PPUCTRL so specific parts can be modified at once
world: .res 2 ;World Data pointer
spriteIndex: .res 1 ;Index of sprite being rendered (should not excede $3F)
metaspritePointer: .res 2 ;Pointer to metasprite data
spritePointer: .res 1 ;Pointer to sprite OAM data at $0200-$02FF (should be in mults of 4... 0,4,8,16,etc.)
entityIndex: .res 1 ;Index for current entity to draw
entityPointer: .res 1 ;Pointer to Entity Graphics data at $0300+
tileStackPointer: .res 1 ;Pointer to TileStack offset ($FF means empty stack)
colorStackPointer: .res 1 ;Pointer to ColorStack offset ($FF means empty stack)


cameraX: .res 2 ;Camera position (world)
cameraXold: .res 2 ;last Camera position (world)
cameraXvel: .res 2 ;Camera position (world)
cameraLead: .res 1 ;Camera Lead room offset (TODO: implement)


screenBoundLeft: .res 1 ;Left screen boundary (do not show screen lower than this)
screenBoundRight: .res 1 ;Right screen boundary (do not show screen this high)
MapColumnTiles: .res 30 ;30 8x8 Tiles Indexed here + 2 bytes map data
MapColumnAttributes: .res 8



;;_________________________________________
;;    GENERAL ASSEMBLY DEFINITIONS
;; (Defining labels with constant values)
;;-----------------------------------------
.DEFINE OAM_FLIP_H %01000000 ;DO NOT CHANGE THESE
.DEFINE OAM_FLIP_V %10000000 ;DO NOT CHANGE THESE

.DEFINE DefaultPPUCTRL %10010000
.DEFINE DefaultPPUMASK %00011000

.DEFINE DrawEntityF_AboveScreen  %00000001
.DEFINE DrawEntityF_AboveWrap    %00000010
.DEFINE DrawEntityF_ScreenSide   %00000100
.DEFINE DrawEntityF_SideWrap     %00001000
.DEFINE DrawEntityF_FarBack      %00010000

;;_________________________________________
;;         ENTITY DATA DEFINITIONS
;;         (Masks for entity data)
;;-----------------------------------------

;;PLAYER DATA
.DEFINE PlayerData_BottomHit     %10000000
.DEFINE PlayerData_TopHit        %01000000
.DEFINE PlayerData_RightHit      %00100000
.DEFINE PlayerData_LeftHit       %00010000
.DEFINE PlayerData_OffScreen     %00001000
.DEFINE PlayerData_LastDirection %00000100
.DEFINE PlayerData_Walking       %00000010

;;_________________________________________
;;            ADDRESS LABELS
;; (Defining labels with constant values)
;;-----------------------------------------



;;  MAP DATA
MapData1 = $0400
MapData2 = $0500
;MapColumnTiles = $0600 ;30 8x8 Tiles Indexed here + 2 bytes map data
;MapColumnAttributes = $0620

;;TILE DATA
TileStack = $0300+TOTALENTITIES ;$20 size stack
;;COLOR DATA
ColorStack = TileStack+$20 ;$18 size stack

;;  GRAPHICS REGISTERS

PPUCTRL = $2000 
;Bits: VPHB SINN	
;NMI enable (V), 
;PPU master/slave (P), 
;sprite height (H), 
;background tile select (B), 
;sprite tile select (S), 
;increment mode (I), 
;nametable select (NN)

PPUMASK = $2001
;Bits: BGRs bMmG
;color emphasis (BGR), 
;sprite enable (s), 
;background enable (b), 
;sprite left column enable (M), 
;background left column enable (m), 
;greyscale (G)

PPUSTATUS = $2002
;Bits: VSO- ----
;vblank (V), 
;sprite 0 hit (S), 
;sprite overflow (O); 
;read resets write pair for $2005/$2006

OAMADDR = $2003
;OAM read/write address

OAMDATA = $2004
;OAM data read/write

PPUSCROLL = $2005
;fine scroll position (two writes: X scroll, Y scroll)

PPUADDR = $2006
;PPU read/write address (two writes: most significant byte, least significant byte)

PPUDATA = $2007
;PPU data read/write

OAMDMA = $4014
;OAM DMA high address





;;___________________________________________________
;;           ROM - STATIC ROM DATA
;; (Data on the cartridge that is always available)
;;       (LIMITED TO 8KB INCLUDING CODE)
;;---------------------------------------------------
;;ROM DATA
.segment "RODATA"

Banktable:              ; Write to this table to switch banks.
  .byte $00, $01, $02, $03, $04, $05, $06
  .byte $07, $08, $09, $0A, $0B, $0C, $0D, $0E
  ; UNROM needs only the first line of this table (0-6)
  ; but UOROM needs both lines (0-14).

PaletteData:
  .byte $22,$29,$1A,$0F,$22,$36,$17,$0f,$22,$30,$21,$0f,$22,$27,$17,$0F  ;background palette data
  .byte $22,$3F,$10,$30,$22,$1A,$30,$27,$22,$16,$30,$27,$22,$0F,$36,$17  ;sprite palette data


;;METASPRITES
.include "GRAPHICS/METASPRITES/RabbitMetasprites.asm"





















;;LEVEL DATA

WorldData:
  .incbin "DATA/world.bin"

MapData:
  .include "DATA/TestMap01.asm"

Map02:
  .include "DATA/TestMap02.asm"





Level1MapPointers:
  .word MapData
  .word Map02
  .word MapData
  .word MapData









MetatileTileRef: ;ORDER: Top left, Bottom Left, Top Right, Bottom Right
  .byte $00, $00, $00, $00 ; 000 - Sky/BG
  .byte $B4, $B6, $B5, $B7 ; 001 - Bottom Ground
  .byte $53, $55, $54, $56 ; 002 - Question Block
  .byte $57, $59, $58, $5A ; 003 - Empty Question Block
  .byte $AB, $AC, $AD, $AE ; 004 - Studded Tile
  .byte $B0, $B1, $B2, $B3 ; 005 - Smiley Cloud
  .byte $A5, $A7, $A6, $A8 ; 006 - Coin
  .byte $45, $47, $45, $47 ; 007 - Brick


MetatileAttrRef:
  .byte $00 ; 000 - Sky/BG
  .byte $01 ; 001 - Bottom Ground
  .byte $03 ; 002 - Question Block
  .byte $03 ; 003 - Empty Question Block
  .byte $01 ; 004 - Studded Tile
  .byte $02 ; 005 - Smiley Cloud
  .byte $03 ; 006 - Coin
  .byte $01 ; 007 - Brick

MetatilePhysRef:
  .byte $00 ; 000 - Sky/BG
  .byte $01 ; 001 - Bottom Ground
  .byte $01 ; 002 - Question Block
  .byte $01 ; 003 - Empty Question Block
  .byte $01 ; 004 - Studded Tile
  .byte $00 ; 005 - Smiley Cloud
  .byte $01 ; 006 - Coin
  .byte $01 ; 007 - Brick


;;_________________________________________
;;         ROM BANKS 0-14 (15x16K)
;;    (ADDITIONAL 16k BANKS OF ROM DATA)
;;-----------------------------------------

;;_________________________________________
;;             BANK 0 $00
;;-----------------------------------------
.segment "BANK0"


;;_________________________________________
;;             BANK 1 $01
;;-----------------------------------------
.segment "BANK1"


;;_________________________________________
;;             BANK 2 $02
;;-----------------------------------------
.segment "BANK2"


;;_________________________________________
;;             BANK 3 $03
;;-----------------------------------------
.segment "BANK3"


;;_________________________________________
;;             BANK 4 $04
;;-----------------------------------------
.segment "BANK4"


;;_________________________________________
;;             BANK 5 $05
;;-----------------------------------------
.segment "BANK5"


;;_________________________________________
;;             BANK 6 $06
;;-----------------------------------------
.segment "BANK6"


;;_________________________________________
;;             BANK 7 $07
;;-----------------------------------------
.segment "BANK7"


;;_________________________________________
;;             BANK 8 $08
;;-----------------------------------------
.segment "BANK8"

;;_________________________________________
;;             BANK 9 $09
;;-----------------------------------------
.segment "BANK9"


;;_________________________________________
;;             BANK 10 $0A
;;-----------------------------------------
.segment "BANK10"


;;_________________________________________
;;             BANK 11 $0B
;;-----------------------------------------
.segment "BANK11"


;;_________________________________________
;;             BANK 12 $0C
;;-----------------------------------------
.segment "BANK12"


;;_________________________________________
;;             BANK 13 $0D
;;-----------------------------------------
.segment "BANK13"


;;_________________________________________
;;             BANK 14 $0E
;;-----------------------------------------
.segment "BANK14"














;;____________________________
;;     Startup ROM Segment
;;----------------------------
.segment "STARTUP"
;;STARTUP/RESET CODE
.include "CODE/StartupRoutine.asm"

















;;___________________________________________________
;;              C     O     D     E
;;           S   E   G   M   E   N  T
;;---------------------------------------------------
.segment "CODE"


WaitVblank:
  LDA #%10000000 ;;Mask for BIT (7th bit for VBLANK)
:
  BIT PPUSTATUS ;;Test Bit 7
  BPL :-
;Stall for time, negate false positives
  RTS


.include "CODE/LibController.asm"
.include "CODE/LoadMetatileTable.asm"
.include "CODE/LoadMetatileColumnNMI.asm"
.include "CODE/LoadTileFromLocation.asm"
.include "CODE/PlayerCollisionCheck.asm"

;;___________________________________________________
;;               CODE - MAIN LOOP
;;       (THE PLACE TO RETURN AFTER NMI)
;;---------------------------------------------------

GameLoop: 
  ;GAME CODE
  .include "CODE/GameCode.asm"

  ;Set GSF Flag to 1 (Game State Finished)
  LDA gameStatus
  AND #%11111110
  CLC
  ADC #$01
  STA gameStatus

CheckGSFFlagLoop:
  ;if GSF flag is 0 (Ready for next game state), goto GameLoop again
  LDA #%00000001
  BIT gameStatus
  BNE :+
  JMP GameLoop
:
  JMP CheckGSFFlagLoop



;;___________________________________________________
;;               CODE - ROUTINES
;;
;;---------------------------------------------------

BankswitchY:
  STY currentBank      ; save the current bank in RAM so the NMI handler can restore it
BankswitchNoSave:
  LDA Banktable, y      ; read a byte from the banktable
  STA Banktable, y      ; and write it back, switching banks
  RTS












;;___________________________________________________
;;           CODE - NON-MASKABLE INTERRUPT
;; (Code Between Frames - Non-Rendered Scanlines 241+)
;;---------------------------------------------------


NMI:
  ;If GSF = 0 (Unready) skip GraphicsTransfer
  LDA #%00000001
  BIT gameStatus
  BEQ SkipGraphicsTransfer

;Reset GSF Flag to 0

  LDA gameStatus
  AND #%11111110
  STA gameStatus

SkipGraphicsTransfer:


  ;;Graphics Code
.include "CODE/GraphicsCode.asm"
  ;;Graphics Code Done


  


NmiReturn:
  LDA #%00000001
  BIT gameStatus
	RTI ;END OF NMI/VBLANK CODE























;;_________________________________________________
;;             CHARACTER ROM - 8k
;;    (Unused for now because of UNROM mapper)
;; (Keep Empty unless using a mapper with CHR ROM)
;;-------------------------------------------------
.segment "CHARS"
TileData:
    .incbin "GRAPHICS/TestTileset.chr"











;;_________________________________________
;;             VECTOR SEGMENT
;; (DON'T CHANGE FOR THE LOVE OF SPAGHETTI)
;;-----------------------------------------
	.segment "VECTORS"
	.word NMI
    .word Reset

;;______________________________________________________________;;
;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;;
;;______________________________________________________________;;
;;                     SPECIAL THANKS TO:                       ;;
;;______________________________________________________________;;
;; -NESDEV.ORG                                                  ;;
;; -The Zero Pages on YouTube by Michael Chiaramonte            ;;
;;                                                              ;;
;;                                                              ;;
;;                                                              ;;
;;                                                              ;;
;;                                                              ;;
;;          (WHICH WITHOUT I COULD NOT HAVE DONE LIKE)          ;;
;;                     (ANY OF THIS AT ALL)                     ;;
;;--------------------------------------------------------------;;
;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~;;
;;______________________________________________________________;;